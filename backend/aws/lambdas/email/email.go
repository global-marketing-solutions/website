package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
)

var (
	sess      = session.Must(session.NewSession())
	sesClient = ses.New(sess)

	sendEmailInput = &ses.SendEmailInput{
		Destination: &ses.Destination{
			ToAddresses: []*string{
				aws.String("info@gms-ai.com"),
			},
		},
		Source: aws.String("info@gms-ai.com"),
	}
	message = &ses.Message{
		Subject: &ses.Content{
			Charset: aws.String("UTF-8"),
			Data:    aws.String("Form from gms-ai.com"),
		},
	}
)

type input struct {
	Name    string
	Message string
	Email   string
}

func (in input) String() string {
	return fmt.Sprintf("Name: %v\n\nMessage: %v\n\nEmail: %v\n", in.Name, in.Message, in.Email)
}

func main() {
	lambda.Start(handleRequest)
}
func handleRequest(ctx context.Context, input input) {
	message.SetBody(
		&ses.Body{
			Text: &ses.Content{
				Charset: aws.String("UTF-8"),
				Data:    aws.String(input.String()),
			},
		},
	)

	sendEmailInput.SetMessage(message)

	// https://docs.aws.amazon.com/sdk-for-go/api/service/ses/#SES.SendEmail
	// https://docs.aws.amazon.com/code-samples/latest/catalog/go-ses-ses_send_email.go.html
	_, err := sesClient.SendEmail(sendEmailInput)
	if err != nil {
		panic(err)
	}
}
