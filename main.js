'use strict';

function enableSlider(blockId) {
    const nodes = {
        'left': document.querySelector(`${blockId} .left`),
        'right': document.querySelector(`${blockId} .right`),
        'dots': document.querySelector(`${blockId} .dots`)
    }
    const _onclick = direction => {
        const nodeCurrent = document.querySelector(`${blockId} .slide.active`);
        const indexCurrent = getIndex(nodeCurrent);
        const slidesCount = nodeCurrent.parentElement.querySelectorAll('.slide').length;

        let indexNew = indexCurrent === 0 ? slidesCount - 1 : indexCurrent - 1;
        if (direction === 'right') {
            indexNew = indexCurrent === slidesCount - 1 ? 0 : indexCurrent + 1;
        }

        slideBlock(blockId, indexNew);
    }
    nodes['left'].onclick = _ => {
        _onclick('left');
    }
    nodes['right'].onclick = _ => {
        _onclick('right');
    }
    nodes['dots'].onclick = event => {
        if (event.target.parentElement.classList.contains('dots')) {
            slideBlock(blockId, getIndex(event.target));
        }
    }
}

window.addEventListener('DOMContentLoaded', _ => {
    const nodes = {}
    enableSlider('#news');
    enableSlider('#we-offer-solutions-for');
    enableSlider('#videos');

    nodes['send'] = document.querySelector('form a');
    nodes['send'].onclick = event => {
        event.preventDefault();

        if (event.target.classList.contains('disabled')) {
            return;
        }

        event.target.classList.add('disabled');

        const progress = document.getElementById('progress');
        progress.classList.add('active');

        fetch('https://8ckadfhy22.execute-api.us-east-1.amazonaws.com/prod/email', {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            'body': new URLSearchParams(new FormData(document.querySelector('form')))
        }).then(resp => {
            if (resp.ok) {
                progress.classList.remove('active');
                alert('Thank you for your message.');
            }
        });
    }
});

function slideBlock(blockId, index) {
    document.querySelector(`${blockId} .slide.active`).classList.remove('active');
    document.querySelectorAll(`${blockId} .slide`)[index].classList.add('active');

    document.querySelector(`${blockId} .dots .current`).classList.remove('current');
    document.querySelector(`${blockId} .dots`).children[index].classList.add('current');
}

function getIndex(node) {
    let i = 0;
    const nodes = node.classList.contains('slide') ? node.parentElement.querySelectorAll('.slide') :
        node.parentElement.children;
    for (const _node of nodes) {
        if (node === _node) {
            return i;
        }
        i++;
    }
}
